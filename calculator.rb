class Calculator
	def initialize(name,weight,print)
		@name = name
		@weight = weight
		@print = print
		puts "Calculator #{@name} Created!"
	end
	def add(a,b)
		if @weight > 1
			a = a*2
		end
		result = a + b
		puts "The added results of #{a} + #{b} is #{result}"
	end
	def subtract(a,b)
		puts a-b
	end
	def multiply(a,b)
		puts a*b
	end
	def divide(a,b)
		return if b is 0
		puts a/b
	end
	def square(a)
		puts a*a
	end
end